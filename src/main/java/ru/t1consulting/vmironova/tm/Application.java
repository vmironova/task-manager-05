package ru.t1consulting.vmironova.tm;

import ru.t1consulting.vmironova.tm.constant.TerminalConst;

import static ru.t1consulting.vmironova.tm.constant.TerminalConst.VERSION;
import static ru.t1consulting.vmironova.tm.constant.TerminalConst.ABOUT;
import static ru.t1consulting.vmironova.tm.constant.TerminalConst.HELP;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showErrorArgument();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void showErrorArgument() {
        System.out.println("Error! This argument not supported...");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Veronika Mironova");
        System.out.println("E-mail: vmironova@t1-consulting.ru");
        System.out.println("E-mail: mironovavg2@vtb.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application version. \n", VERSION);
        System.out.printf("%s - Show developer info. \n", ABOUT);
        System.out.printf("%s - Show application commands. \n", HELP);
    }
}
